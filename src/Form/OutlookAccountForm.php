<?php

namespace Drupal\outlook_calendar\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides the form for adding outlook account.
 */
class OutlookAccountForm extends FormBase {

  /**
   * Database connection object.
   *
   * @var Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The message interface.
   *
   * @var Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The RequestStack service.
   *
   * @var Symfony\Componenet\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * AccountForm constructor.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack service.
   * @param Drupal\Core\Messenger\MessengerInterface $messenger_interface
   *   The messenger interface service.
   */
  public function __construct(Connection $database, RequestStack $request_stack, MessengerInterface $messenger_interface) {
    $this->database = $database;
    $this->requestStack = $request_stack;
    $this->messenger = $messenger_interface;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
          $container->get('database'),
          $container->get('request_stack'),
          $container->get('messenger')
      );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'outlook_account_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $record = [];
    $num = $this->requestStack->getCurrentRequest()->query->get('num');
    if (isset($num)) {
      $query = $this->database->select('outlook_calendar', 'oe')
        ->condition('id', $num)->fields('oe');
      $record = $query->execute()
        ->fetchAssoc();
    }
    $form['mail'] = [
      '#type' => 'email',
      '#title' => $this->t('Exchange Account MailID'),
      '#required' => TRUE,
      '#default_value' => (isset($record['mail']) && $num) ? $record['mail'] : '',
    ];
    $form['password'] = [
      '#type' => 'password',
      '#title' => $this->t('Exchange MailID Password'),
      '#required' => TRUE,
      '#default_value' => (isset($record['password']) && $num) ? $record['password'] : '',
    ];
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#default_value' => (isset($num)) ? $this->t('Update Account') : $this->t('Add Account'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $mail = $form_state->getValue('mail');
    $num = $this->requestStack->getCurrentRequest()->query->get('num');
    $query = $this->database->select('outlook_calendar', 'oe');
    $query->fields('oe', ['mail']);
    $query->condition('mail', $mail, '=');
    $result = $query->execute()
      ->fetchAll();
    try {
      $result = $query->execute()
        ->fetchAll();
    }
    catch (exception $e) {
      return $e->getMessage();
    }
    if (!empty($result) && empty($num)) {
      $form_state->setErrorByName('mail', $this->t('This outlook account id already exists. Please enter a unique outlook account id'));
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $num = $this->requestStack->getCurrentRequest()->query->get('num');
    $account = $this->currentUser();
    $uid = $account->id();
    $field = $form_state->getValues();
    $re_url = Url::fromRoute('outlook_calendar.account');
    $mail = $field['mail'];
    // Encrypt the password before storing it in database.
    $password = base64_encode($field['password']);
    if (isset($num)) {
      $field = [
        'mail' => $mail,
        'password' => $password,
      ];
      $this->database->update('outlook_calendar')
        ->fields($field)->condition('id', $num)->execute();
      $this->messenger->addStatus($this->t('The Outlook Account has been succesfully updated.'));
      $form_state->setRedirectUrl($re_url);
    }
    else {
      $field = [
        'mail' => $mail,
        'password' => $password,
        'uid' => $uid,
      ];
      $this->database->insert('outlook_calendar')
        ->fields($field)->execute();
      $this->messenger->addStatus($this->t('The Outlook Account has been succesfully saved.'));
      $form_state->setRedirectUrl($re_url);
    }
  }

}
